FROM ubuntu:22.04

RUN apt update
RUN apt install -y python3 python3-pip curl bash git
RUN python3 -m pip install --no-input ansible
